class HabraSpider(scrapy.Spider):
    name = 'habra'

    def __init__(self, start_urls=None, *args, **kwargs):
        if start_urls is not None:
           self._start_urls = start_urls
        # else:
        #     self._start_urls = []
        super(HabraSpider, self).__init__(*args, **kwargs)

process = CrawlerProcess({
    'FEED_FORMAT': 'json',
    'FEED_URI': 'habra_info.json',
    'LOG_ENABLED': False,
})

habra_hub_links_data = json.load(open("habra_hub_links.json", 'r'))  # Here the URLs are uploaded from disk
habra_hub_urls = [item["hub_url"] for item in habra_hub_links_data]

spider = HabraSpider(start_urls=habra_hub_urls)
process.crawl(spider)
process.start()