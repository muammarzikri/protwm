open(my $env,'>:encoding(UTF-8)',".env");
my %enve=(
	"BASE_PATH"=>removeNewLine(`pwd`)
);
$enve{"SRC_PATH"}=$enve{"BASE_PATH"}."/src";
$enve{"DATA_PATH"}=$enve{"BASE_PATH"}."/data";


for my $key (keys %enve){
	print $env $key."=".$enve{$key}."\n";
}
close $env;

sub removeNewLine{
	my $string = $_[0]; 
	$string =~ s/\n//g;
	print $string;
 	return $string;
}